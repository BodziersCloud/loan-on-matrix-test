package pl.finmatik.loanonmatrixtest.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import pl.finmatik.loanonmatrixtest.model.AppUser;

import java.util.Optional;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {
    Optional<AppUser> findByEmail(String email);

    boolean existsByEmail(String email);
}
