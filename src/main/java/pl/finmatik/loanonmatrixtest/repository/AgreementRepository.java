package pl.finmatik.loanonmatrixtest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.finmatik.loanonmatrixtest.model.Agreement;

@Repository
public interface AgreementRepository extends JpaRepository<Agreement, Long> {

}
