package pl.finmatik.loanonmatrixtest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.finmatik.loanonmatrixtest.model.MoneyLender;

import java.util.List;

public interface MoneyLenderRepository extends JpaRepository<MoneyLender, Long> {

}
