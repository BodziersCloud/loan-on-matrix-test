package pl.finmatik.loanonmatrixtest.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.finmatik.loanonmatrixtest.model.LoanLaunch;


@Repository
public interface LoanLaunchRepository extends JpaRepository<LoanLaunch, Long> {


}
