package pl.finmatik.loanonmatrixtest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.finmatik.loanonmatrixtest.model.LoanRepayment;


@Repository
public interface LoanRepaymentRepository extends JpaRepository<LoanRepayment, Long> {

}
