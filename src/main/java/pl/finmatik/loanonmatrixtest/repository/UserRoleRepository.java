package pl.finmatik.loanonmatrixtest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.finmatik.loanonmatrixtest.model.UserRole;

public interface UserRoleRepository extends JpaRepository<UserRole, Long> {
    boolean existsByName(String name);

    UserRole findByName(String role);
}
