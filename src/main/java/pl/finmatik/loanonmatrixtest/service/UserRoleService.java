package pl.finmatik.loanonmatrixtest.service;


import pl.finmatik.loanonmatrixtest.model.UserRole;

import java.util.Set;

public interface UserRoleService {
    Set<UserRole> getDefaultUserRoles();
}
