package pl.finmatik.loanonmatrixtest.service;

import pl.finmatik.loanonmatrixtest.model.Agreement;
import pl.finmatik.loanonmatrixtest.model.MoneyLender;

import java.util.List;
import java.util.Optional;

public interface AgreementService {

    void registerAgreement(Agreement agreement, Long moneyLenderId);
    List<Agreement> getAllAgreements();
    Optional<Agreement> getAgreementById(Long id);
    void updateAgreement(Agreement agreement, MoneyLender moneyLender);
    void removeAgreementById(Long id);
    List<Agreement> getAllByLender(Long lenderId);

}
