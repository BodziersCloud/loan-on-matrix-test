package pl.finmatik.loanonmatrixtest.service;

import pl.finmatik.loanonmatrixtest.model.LoanRepayment;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface LoanRepaymentService {

    List<LoanRepayment> repaymentListForAgreementId(Long agreementId);
    void addRepaymentToAgreement(Long agreementId, LoanRepayment loanRepayment);

    void updateRepayment(LoanRepayment loanRepayment);
    List<LoanRepaymentService> loanRepaymentListByDate(LocalDate date);
    List<LoanRepaymentService> loanRepaymentListInDatesRange(LocalDate startDate, LocalDate endDate);

    void removeRepaymentById(Long repaymentId);

    Optional<LoanRepayment> getRepaymentById(Long repaymentIdd);

    List<LoanRepayment> getAllRepayments();
}
