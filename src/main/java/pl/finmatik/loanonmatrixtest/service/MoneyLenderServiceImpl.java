package pl.finmatik.loanonmatrixtest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.finmatik.loanonmatrixtest.exception.LenderNotFoundException;
import pl.finmatik.loanonmatrixtest.model.MoneyLender;
import pl.finmatik.loanonmatrixtest.repository.MoneyLenderRepository;

import java.util.List;
import java.util.Optional;

@Service
public class MoneyLenderServiceImpl implements MoneyLenderService {

    private MoneyLenderRepository moneyLenderRepository;

    @Autowired
    public MoneyLenderServiceImpl(MoneyLenderRepository moneyLenderRepository) {
        this.moneyLenderRepository = moneyLenderRepository;
    }

    @Override
    public List<MoneyLender> getAllMoneyLenders() {
        return moneyLenderRepository.findAll();
    }

    @Override
    public Optional<MoneyLender> getMoneyLenderById(Long id) {
    if(!moneyLenderRepository.findById(id).isPresent()) {
        throw new LenderNotFoundException("No such money lender");
    }
        return moneyLenderRepository.findById(id);
    }

    @Override
    public void updateMoneyLender(MoneyLender moneyLender) {
        moneyLenderRepository.save(moneyLender);
    }

    @Override
    public void removeMoneyLenderById(Long id) {
        moneyLenderRepository.deleteById(id);
    }
}
