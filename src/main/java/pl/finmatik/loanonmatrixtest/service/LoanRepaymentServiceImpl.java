package pl.finmatik.loanonmatrixtest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.finmatik.loanonmatrixtest.exception.AgreementNotFoundException;
import pl.finmatik.loanonmatrixtest.model.Agreement;
import pl.finmatik.loanonmatrixtest.model.LoanRepayment;
import pl.finmatik.loanonmatrixtest.repository.AgreementRepository;
import pl.finmatik.loanonmatrixtest.repository.LoanRepaymentRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class LoanRepaymentServiceImpl implements LoanRepaymentService {

    private LoanRepaymentRepository loanRepaymentRepository;
    private AgreementRepository agreementRepository;

    @Autowired
    public LoanRepaymentServiceImpl(LoanRepaymentRepository loanRepaymentRepository, AgreementRepository agreementRepository) {
        this.loanRepaymentRepository = loanRepaymentRepository;
        this.agreementRepository = agreementRepository;
    }


    @Override
    public List<LoanRepayment> repaymentListForAgreementId(Long agreementId) {
        List<LoanRepayment> loanRepaymentList = loanRepaymentRepository.findAll();
        loanRepaymentList.stream()
                .filter(x -> agreementId.equals(x.getAgreement().getId()))
                .collect(Collectors.toList());
        return loanRepaymentList;
    }

    @Override
    public void addRepaymentToAgreement(Long agreementId, LoanRepayment loanRepayment) {
        Optional<Agreement> agreementOptional = agreementRepository.findById(agreementId);
        if(!agreementOptional.isPresent()) {
            throw new AgreementNotFoundException("No such agreement with lenderId: " +agreementId );
        }

        Agreement agreement = agreementOptional.get();
        agreement.getLoanRepaymentsList().add(loanRepayment);
        loanRepayment.setAgreement(agreement);

        loanRepaymentRepository.save(loanRepayment);
        agreementRepository.save(agreement);
    }

    @Override
    public void updateRepayment(LoanRepayment loanRepayment) {
        loanRepaymentRepository.save(loanRepayment);
    }


    @Override
    public List<LoanRepaymentService> loanRepaymentListByDate(LocalDate date) {
        return null;
    }

    @Override
    public List<LoanRepaymentService> loanRepaymentListInDatesRange(LocalDate startDate, LocalDate endDate) {
        return null;
    }

    @Override
    public void removeRepaymentById(Long repaymentId) {
        loanRepaymentRepository.deleteById(repaymentId);
    }

    @Transactional
    @Override
    public Optional<LoanRepayment> getRepaymentById(Long repaymentId) {
        Optional<LoanRepayment> repaymentOptional = loanRepaymentRepository.findById(repaymentId);
        return repaymentOptional;
    }

    @Override
    public List<LoanRepayment> getAllRepayments() {
        List<LoanRepayment> repayments = loanRepaymentRepository.findAll();
        return repayments;
    }



}

