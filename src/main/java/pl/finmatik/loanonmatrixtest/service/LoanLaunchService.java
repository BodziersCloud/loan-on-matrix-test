package pl.finmatik.loanonmatrixtest.service;


import pl.finmatik.loanonmatrixtest.model.Agreement;
import pl.finmatik.loanonmatrixtest.model.LoanLaunch;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface LoanLaunchService {

    List<LoanLaunch> launchListForAgreementId (Long agreementId);
    List<LoanLaunch> listAllLoanLaunches();
//    void addLaunchToAgreement(Long agreementId, LoanLaunch loanLaunch);

    void deleteLaunch(Long launchId);
    void updateLaunch(LoanLaunch loanLaunch, Agreement agreement);
    List<LoanLaunch> loanLaunchListByDate(LocalDate lauchDate);
    List<LoanLaunch> loanLaunchListInDatesRange(LocalDate startDate, LocalDate endDate);

    Optional<LoanLaunch> getById(Long id);

}
