package pl.finmatik.loanonmatrixtest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.finmatik.loanonmatrixtest.exception.AgreementNotFoundException;
import pl.finmatik.loanonmatrixtest.model.Agreement;
import pl.finmatik.loanonmatrixtest.model.LoanLaunch;
import pl.finmatik.loanonmatrixtest.repository.AgreementRepository;
import pl.finmatik.loanonmatrixtest.repository.LoanLaunchRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class LoanLaunchServiceImpl implements LoanLaunchService {

    private LoanLaunchRepository loanLaunchRepository;
    private AgreementRepository agreementRepository;

    @Autowired
    public LoanLaunchServiceImpl(LoanLaunchRepository loanLaunchRepository, AgreementRepository agreementRepository) {
        this.loanLaunchRepository = loanLaunchRepository;
        this.agreementRepository = agreementRepository;
    }
    @Transactional
    @Override
    public List<LoanLaunch> launchListForAgreementId(Long agreementId) {
        List<LoanLaunch> loanLaunchList = loanLaunchRepository.findAll();
        loanLaunchList.stream()
                .filter(x -> agreementId.equals(x.getAgreement().getId()))
                .collect(Collectors.toList());
        return loanLaunchList;
    }

    @Override
    public List<LoanLaunch> listAllLoanLaunches() {
        return loanLaunchRepository.findAll();
    }
//    @Transactional
//    @Override
//    public void addLaunchToAgreement(Long agreementId, LoanLaunch loanLaunch) {
//        Optional<Agreement> agreementOptional = agreementRepository.findById(agreementId);
//        if(!agreementOptional.isPresent()) {
//            throw new AgreementNotFoundException("No such agreement with lenderId: " +agreementId );
//        }
//
//        Agreement agreement = agreementOptional.get();
//        agreement.getLoanLaunchesList().add(loanLaunch);
//        loanLaunch.setAgreement(agreement);
//
//        agreementRepository.save(agreement);
//        loanLaunchRepository.save(loanLaunch);
//    }
    @Transactional
    @Override
    public void deleteLaunch(Long launchId) {
        loanLaunchRepository.deleteById(launchId);
    }
    @Transactional
    @Override
    public void updateLaunch(LoanLaunch loanLaunch, Agreement agreement) {
        loanLaunchRepository.save(loanLaunch);
    }

    @Transactional
    @Override
    public List<LoanLaunch> loanLaunchListByDate(LocalDate lauchDate) {
        List<LoanLaunch> loanLaunchByDateList = loanLaunchRepository.findAll();
        loanLaunchByDateList.stream()
                .filter(x -> lauchDate.equals(x.getLaunchDate()))
                .collect(Collectors.toList());
        return loanLaunchByDateList;
    }

    @Override
    public List<LoanLaunch> loanLaunchListInDatesRange(LocalDate startDate, LocalDate endDate) {
        return null;
    }
    @Transactional
    @Override
    public Optional<LoanLaunch> getById(Long launchId) {
        return loanLaunchRepository.findById(launchId);
    }
}
