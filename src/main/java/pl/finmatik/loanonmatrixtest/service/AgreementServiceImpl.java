package pl.finmatik.loanonmatrixtest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.finmatik.loanonmatrixtest.exception.AgreementNotFoundException;
import pl.finmatik.loanonmatrixtest.exception.LenderNotFoundException;
import pl.finmatik.loanonmatrixtest.model.Agreement;
import pl.finmatik.loanonmatrixtest.model.MoneyLender;
import pl.finmatik.loanonmatrixtest.repository.AgreementRepository;
import pl.finmatik.loanonmatrixtest.repository.MoneyLenderRepository;

import java.util.List;
import java.util.Optional;

@Service
public class AgreementServiceImpl implements AgreementService {


    private AgreementRepository agreementRepository;
    private MoneyLenderRepository moneyLenderRepository;

    //autowire constructor so bean is registered to DepencyInjection

    @Autowired
    public AgreementServiceImpl(AgreementRepository agreementRepository, MoneyLenderRepository moneyLenderRepository) {
        this.agreementRepository = agreementRepository;
        this.moneyLenderRepository = moneyLenderRepository;
    }

    @Transactional
    @Override
    public void registerAgreement(Agreement agreement, Long moneyLenderId) {
         Optional<MoneyLender> optionalMoneyLender = moneyLenderRepository.findById(moneyLenderId);

         if (optionalMoneyLender.isPresent()) {
             agreement.setMoneyLender(optionalMoneyLender.get());
         }
         agreementRepository.save(agreement);
    }

    //List all agreements
    @Override
    public List<Agreement> getAllAgreements() {
        return agreementRepository.findAll();
    }

    //READ operation

    @Transactional
    @Override
    public Optional<Agreement> getAgreementById(Long id) {
        if (!agreementRepository.findById(id).isPresent()) {
            throw new AgreementNotFoundException("No such agreement");
        }
        return agreementRepository.findById(id);
    }

    //there will be option to update only one agreement
    //UPDATE operation
    @Transactional
    @Override
    public void updateAgreement (Agreement agreement, MoneyLender moneyLender) {
        agreementRepository.save(agreement);

    }
    //DELETE operation
    @Transactional
    @Override
    public void removeAgreementById(Long id) {
        agreementRepository.deleteById(id);
    }

    @Override
    public List<Agreement> getAllByLender(final Long lenderId) {
       Optional<MoneyLender> moneyLenderOptional = moneyLenderRepository.findById(lenderId);

       if(!moneyLenderOptional.isPresent()) {
           throw new LenderNotFoundException("No such lender");
       }
       return moneyLenderOptional.get().getAgreementList();
    }
}
