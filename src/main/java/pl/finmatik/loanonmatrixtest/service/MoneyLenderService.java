package pl.finmatik.loanonmatrixtest.service;

import pl.finmatik.loanonmatrixtest.model.Agreement;
import pl.finmatik.loanonmatrixtest.model.MoneyLender;

import java.util.List;
import java.util.Optional;

public interface MoneyLenderService {
    Optional<MoneyLender> getMoneyLenderById(Long id);
    void updateMoneyLender(MoneyLender moneyLender);
    void removeMoneyLenderById(Long id);
    List<MoneyLender> getAllMoneyLenders();
}
