package pl.finmatik.loanonmatrixtest.service;


import pl.finmatik.loanonmatrixtest.model.AppUser;

import java.util.List;

public interface UserService {
    void registerUser(String username, String password, String passwordConfirm);
    List<AppUser> getAllUsers();

}
