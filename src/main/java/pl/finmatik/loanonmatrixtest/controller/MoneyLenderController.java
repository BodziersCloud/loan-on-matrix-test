package pl.finmatik.loanonmatrixtest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.finmatik.loanonmatrixtest.model.MoneyLender;
import pl.finmatik.loanonmatrixtest.service.MoneyLenderService;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping(path = "/user/lender")
public class MoneyLenderController {


    @Autowired
    private MoneyLenderService moneyLenderService;

    //    //################################ ADD
    @GetMapping(path = "/add")
    public String addMoneyLender(Model model) {
        model.addAttribute("moneyLender", new MoneyLender());
        return "lenderForm";
    }

    //    ################################ LIST LENDERS
    @GetMapping(path = "/list")
    public String listMoneyLender(Model model) {
        model.addAttribute("moneyLenderList", moneyLenderService.getAllMoneyLenders());
        return "lenderList";
    }

    //    ################################ SHOWING DETAILS
    @GetMapping(path = "/details")
    public String moneyLenderDetails(@RequestParam(name = "moneyLenderId") Long id, Model model) {
        Optional<MoneyLender> moneyLenderOptional = moneyLenderService.getMoneyLenderById(id);
        if (moneyLenderOptional.isPresent()) {
            // mamy mieszkanie, obsługa i wyświetlenie
            model.addAttribute("moneyLender",moneyLenderOptional.get());
            return "lenderDetails";
        }
        // nie ma mieszkania, cóż zrobić?
        return "redirect:/user/lender/list";
    }

    //    ################################ SAVE
    // SAVE DZIAŁA PRZY ZAPISIE/DODANIU ORAZ ZAPISIE/UPDATE
    @PostMapping(path = "/save")
    public String updateMoneyLender(Model model, @Valid MoneyLender moneyLender) {
        try {
            moneyLenderService.updateMoneyLender(moneyLender);
        } catch (Exception e) {
            model.addAttribute("moneyLender", moneyLender);
            return "lenderForm";
        }
        return "redirect:/user/lender/list";
    }


    // ################################ DELETE
    @GetMapping(path = "/delete")
    public String deleteApartment(@RequestParam(name = "moneyLenderId") Long id) {
        moneyLenderService.removeMoneyLenderById(id);
        return "redirect:/user/lender/list";
    }

    //################################ UPDATE
    @GetMapping(path = "/update")
    public String updateForm(@RequestParam(name = "moneyLenderId") Long id, Model model) {
        Optional<MoneyLender> moneyLenderOptional = moneyLenderService.getMoneyLenderById(id);
        if (moneyLenderOptional.isPresent()) {
            model.addAttribute("moneyLender", moneyLenderOptional.get());
            return "lenderForm";
        }
        return "redirect:/user/lender/list";
    }


}
