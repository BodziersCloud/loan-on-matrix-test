package pl.finmatik.loanonmatrixtest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.finmatik.loanonmatrixtest.model.Agreement;
import pl.finmatik.loanonmatrixtest.model.LoanLaunch;

import pl.finmatik.loanonmatrixtest.service.AgreementService;
import pl.finmatik.loanonmatrixtest.service.LoanLaunchService;
import pl.finmatik.loanonmatrixtest.service.LoanRepaymentService;

import java.util.Optional;

@Controller
@RequestMapping(path = "/user/launch")
public class LoanLaunchController {

    @Autowired
    private LoanLaunchService loanLaunchService;
    @Autowired
    private AgreementService agreementService;
    @Autowired
    private LoanRepaymentService loanRepaymentService;


    //    ################################ LIST LOAN LAUNCHES
    @GetMapping(path = "/list")
    public String listAlLoanLaunch(Model model) {
        model.addAttribute("loanLaunchList", loanLaunchService.listAllLoanLaunches());
        //        model.addAttribute("agreementList", agreementService.getAllAgreements());
        return "launchesList";
    }

    //    ################################ SHOWING DETAILS OF LOAN LAUNCH
    @GetMapping(path = "/details")
    public String loanLaunchDetails(@RequestParam(name = "loanLaunchId") Long launchId, Model model) {
        Optional<LoanLaunch> loanLaunchOptional = loanLaunchService.getById(launchId);
        if (loanLaunchOptional.isPresent()) {
            // mamy uruchomienie, obsługa i wyświetlenie
            model.addAttribute("loanLaunch", loanLaunchOptional.get());
            return "loanLaunchDetails";
        }
        // redirect if launch is not present
        return "redirect:/user/launch/list";
    }

    //    //################################ ADD LAUNCH
    @GetMapping(path = "/add")
    public String addLaunchToAgreement(Model model) {
        model.addAttribute("loanLaunch" , new LoanLaunch());
        model.addAttribute("agreementList", agreementService.getAllAgreements());
        return "launchForm";
    }

    //    ################################ SAVE
    // SAVE DZIAŁA PRZY ZAPISIE/DODANIU ORAZ ZAPISIE/UPDATE
    @PostMapping(path = "/save")
    public String updateLaunch(Model model, LoanLaunch loanLaunch, Agreement agreement) {
        try {
            loanLaunchService.updateLaunch(loanLaunch, agreement);
        } catch (Exception e) {
            model.addAttribute("loanLaunch", loanLaunch);
            model.addAttribute("agreement", agreement);
            return "launchForm";
        }
        return "redirect:/user/launch/list";
    }

    // ################################ DELETE
    @GetMapping(path = "/delete")
    public String deleteLaunch(@RequestParam(name = "loanLaunchId") Long launchId) {
        loanLaunchService.deleteLaunch(launchId);
        return "redirect:/user/launch/list";
    }

    //################################ UPDATE
    @GetMapping(path = "/update")
    public String updateLaunchForm(@RequestParam(name = "loanLaunchId") Long launchId, Model model) {
        Optional<LoanLaunch> optionalLoanLaunch = loanLaunchService.getById(launchId);

        Optional<Agreement> agreementOptional = agreementService.getAgreementById(optionalLoanLaunch.get().getAgreement().getId());
         if (optionalLoanLaunch.isPresent()) {
            model.addAttribute("loanLaunch", optionalLoanLaunch.get());
            model.addAttribute("agreement", agreementOptional.get());
            model.addAttribute("agreementList", agreementService.getAllAgreements());

            return "launchForm";
        }
        return "redirect:/user/launch/list";
    }



}
