package pl.finmatik.loanonmatrixtest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.finmatik.loanonmatrixtest.service.UserService;


@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/formbasic")
    public String getBasicForm() {
        return "form-basic"; //loading basic form themeplate to check all dependencies
    }

//  users will not be able to register themself, only admin will be allowed to register new users

//    @GetMapping("/register")
//    public String getRegisterForm() {
//        return "register"; // załaduj widok userRegister (formularz)
//    }
//
//    @PostMapping("/register")
//    public String submitRegisterForm(@RequestParam(name = "username") String username,
//                                     @RequestParam(name = "password") String password,
//                                     @RequestParam(name = "password-confirm") String passwordConfirm){
//        userService.registerUser(username, password, passwordConfirm); // zapisz użytkownika w bazie
//
//        return "redirect:/login"; // przekieruj
//    }

}
