package pl.finmatik.loanonmatrixtest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.finmatik.loanonmatrixtest.model.Agreement;
import pl.finmatik.loanonmatrixtest.model.LoanRepayment;
import pl.finmatik.loanonmatrixtest.service.AgreementService;
import pl.finmatik.loanonmatrixtest.service.LoanRepaymentService;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping(path = "/user/repayment")
public class LoanRepaymentController {

    @Autowired
    private LoanRepaymentService loanRepaymentService;

    @Autowired
    private AgreementService agreementService;

    //################################ ADD
    @GetMapping(path = "/add")
    public String repaymentForm(Model model) {
        model.addAttribute("repayment", new LoanRepayment());
        model.addAttribute("agreement", new Agreement());
        return "repaymentForm";
    }
    //################################ ADD

    //################################ DELETE
        @GetMapping(path = "/delete")
        public String deleteRepayment(@RequestParam(name = "repaymentId") Long id) {
        loanRepaymentService.removeRepaymentById(id);
        return "redirect:/repayment/list";
    }
    //################################ DELETE


    //################################ UPDATE
    @GetMapping(path = "/update")
    public String repaymentForm(@RequestParam(name = "repaymentId") Long id, Model model) {
        Optional<LoanRepayment> repaymentOptional = loanRepaymentService.getRepaymentById(id);
        if (repaymentOptional.isPresent()) {
            model.addAttribute("repayment", repaymentOptional.get());
            return "repaymentForm";
        }
        return "redirect:/apartment/list";
    }
//    ################################ UPDATE


        //    ################################ SAVE

    // SAVE DZIAŁA PRZY ZAPISIE/DODANIU ORAZ ZAPISIE/UPDATE
    @PostMapping(path = "/save")
    public String updateRepayment(Model model, @Valid LoanRepayment repayment) {
        try {
            loanRepaymentService.updateRepayment(repayment);
        } catch (Exception e) {
            model.addAttribute("repayment", repayment);
            return "repaymentForm";
        }
        return "redirect:/repayment/list";
    }
//        ################################ SAVE


//        ################################ LIST

    @GetMapping(path = "/list")
    public String listRepayments(Model model) {
        model.addAttribute("repaymentList", loanRepaymentService.getAllRepayments());
        return "repaymentList";
    }
//        ################################ LIST

//        ################################ FILTERS
//        ################################


}
