package pl.finmatik.loanonmatrixtest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.finmatik.loanonmatrixtest.model.Agreement;
import pl.finmatik.loanonmatrixtest.model.LoanLaunch;
import pl.finmatik.loanonmatrixtest.model.LoanRepayment;
import pl.finmatik.loanonmatrixtest.model.MoneyLender;
import pl.finmatik.loanonmatrixtest.service.AgreementService;
import pl.finmatik.loanonmatrixtest.service.LoanLaunchService;
import pl.finmatik.loanonmatrixtest.service.LoanRepaymentService;
import pl.finmatik.loanonmatrixtest.service.MoneyLenderService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(path = "/user/agr")
public class AgreementController {

    @Autowired
    private AgreementService agreementService;
    @Autowired
    private MoneyLenderService moneyLenderService;
    @Autowired
    private LoanRepaymentService loanRepaymentService;
    @Autowired
    private LoanLaunchService loanLaunchService;


    //    //################################ ADD
    @GetMapping(path = "/add")
    public String addAgreement(Model model) {
        model.addAttribute("agreement", new Agreement());
        model.addAttribute("moneyLenderList", moneyLenderService.getAllMoneyLenders());
        return "agreementForm";
    }

    //    ################################ LIST AGREMENTS
    @GetMapping(path = "/list")
    public String listAgreement(Model model) {
        model.addAttribute("agreementList", agreementService.getAllAgreements());
        return "agreementList";
    }

    //    ################################ SHOWING DETAILS
    @GetMapping(path = "/details")
    public String agreementDetails(@RequestParam(name = "agreementId") Long id, Model model) {
        Optional<Agreement> agreementOptional = agreementService.getAgreementById(id);
        if (agreementOptional.isPresent()) {
            // mamy umowę, obsługa i wyświetlenie
            model.addAttribute("agreement", agreementOptional.get());
            return "agreementDetails";
        }
        // nie ma umowy, cóż zrobić?
        return "redirect:/user/agr/list";
    }

    //    ################################ SAVE
    // SAVE DZIAŁA PRZY ZAPISIE/DODANIU ORAZ ZAPISIE/UPDATE
    @PostMapping(path = "/save")
    public String updateAgreement(Model model, @Valid Agreement agreement, @Valid MoneyLender moneyLender) {
        try {
            agreementService.updateAgreement(agreement, moneyLender);
        } catch (Exception e) {
            model.addAttribute("agreement", agreement);
            model.addAttribute("moneyLender", moneyLender);
            return "agreementForm";
        }
        return "redirect:/user/agr/list";
    }


    // ################################ DELETE FROM EDIT FORM
    @GetMapping(path = "/delete")
    public String deleteApartment(@RequestParam(name = "agreementId") Long id) {
        agreementService.removeAgreementById(id);
        return "redirect:/user/agr/list";
    }

    //################################ UPDATE

    //    i will have to consider builiding agreement object to save by request params
    @GetMapping(path = "/update")
    public String updateForm(@RequestParam(name = "agreementId") Long agreementId, Model model) {
        Optional<Agreement> agreementOptional = agreementService.getAgreementById(agreementId);

//        pytanie czy ręcznie muszę dbać o tą zależność nawet jeśli chcę ją zmienić potem w formularzu czy będzie obsługiwana automagicznie?
//        ptanie dwa, ja przekazać lenderId do formularza i czy będzie tam dostepne pole lender.lenderId?
        Optional<MoneyLender> moneyLenderOptional = moneyLenderService.getMoneyLenderById(agreementOptional.get().getMoneyLender().getLenderId());


        if (agreementOptional.isPresent()) {
            model.addAttribute("agreement", agreementOptional.get());
            model.addAttribute("lender", moneyLenderOptional.get());
            model.addAttribute("moneyLenderList", moneyLenderService.getAllMoneyLenders());
            model.addAttribute("loanLaunchList", loanLaunchService.launchListForAgreementId(agreementId));
            model.addAttribute("loanRepaymentList", loanRepaymentService.repaymentListForAgreementId(agreementId));
            return "agreementForm";
        }
        return "redirect:/user/agr/list";
    }

    //################################ UPDATE AGREEMENT WITH LOAN LAUNCH AND REPAYMENT
    @GetMapping(path = "/service")
    public String listWithLaunchRepayment(@RequestParam(name = "agreement-lenderId") Long agreementId, Model model) {
        Optional<Agreement> optionalAgreement = agreementService.getAgreementById(agreementId);
        if (optionalAgreement.isPresent()) {
            List<LoanRepayment> loanRepaymentsList = optionalAgreement.get().getLoanRepaymentsList();
            List<LoanLaunch> loanLaunchesList = optionalAgreement.get().getLoanLaunchesList();
        }
        return "agreementService";
    }


}
