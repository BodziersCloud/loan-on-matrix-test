package pl.finmatik.loanonmatrixtest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/")
public class IndexController {


//    login page - main address
    @GetMapping(path = "/login")
    public String login() {
        return "login";
    }

    //    first page with system description after success login
    @GetMapping(path = "/index")
    public String index() {
        return "index";
    }

    //logout redirect back to login
    @GetMapping("/*")
    public String logout() {
        return "login";
    }



}
