package pl.finmatik.loanonmatrixtest.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.finmatik.loanonmatrixtest.service.UserService;

@Controller
@RequestMapping("/admin/")
public class AdminController {


    @Autowired
    private UserService userService;

    @GetMapping("/user/list")
    public String getUserList(Model model) {
        model.addAttribute("userList", userService.getAllUsers());
        return "userList";
    }
    //admin will register users - displays register page for admin
    @GetMapping("/user/register")
    public String getRegisterForm() {
        return "register"; // załaduj widok userRegister (formularz)
    }


    //admin will send new user form to registration (with default role which will be changed on user list)
    @PostMapping("/user/register")
    public String submitRegisterForm(@RequestParam(name = "username") String username,
                                     @RequestParam(name = "password") String password,
                                     @RequestParam(name = "password-confirm") String passwordConfirm){
        userService.registerUser(username, password, passwordConfirm); // zapisz użytkownika w bazie

        return "redirect:/user/list"; // przekieruj
    }

}
