package pl.finmatik.loanonmatrixtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoanOnMatrixTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoanOnMatrixTestApplication.class, args);
    }

}
