package pl.finmatik.loanonmatrixtest.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanLaunch {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Double launchAmount;

    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private LocalDate launchDate;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE,
            CascadeType.DETACH, CascadeType.REFRESH})
    private Agreement agreement;

}
