package pl.finmatik.loanonmatrixtest.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MoneyLender {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long lenderId;

    private String name;
    private String email;
    private String address;
    private String city;
    private String postalCode;
    private String nip;
    private String regon;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "moneyLender", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE,
            CascadeType.DETACH, CascadeType.REFRESH})
    List<Agreement> agreementList;

}
