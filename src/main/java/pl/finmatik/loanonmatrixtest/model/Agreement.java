package pl.finmatik.loanonmatrixtest.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Agreement {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Double loanAmount;
    private String loanNumber;

    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private LocalDate agreementSignDate;

    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private LocalDate agreementEndDate;
    private Double normalInterests;
    private Double penaltyInterests;
    private boolean activeAgreement;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE,
            CascadeType.DETACH, CascadeType.REFRESH})
    private MoneyLender moneyLender;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "agreement", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE,
            CascadeType.DETACH, CascadeType.REFRESH})
    private List<LoanLaunch> loanLaunchesList;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "agreement", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE,
            CascadeType.DETACH, CascadeType.REFRESH})
    private List<LoanRepayment> loanRepaymentsList;
}
