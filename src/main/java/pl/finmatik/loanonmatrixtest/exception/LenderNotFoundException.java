package pl.finmatik.loanonmatrixtest.exception;

import sun.plugin2.message.Message;

public class LenderNotFoundException extends RuntimeException {
    public LenderNotFoundException(String message) {
        super(message);
    }
}
